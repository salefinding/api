package com.salefinding.api.utils;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.salefinding.models.api.ApCategory;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class EntityToJson {

    public static ObjectNode parseListCategory(List<ApCategory> categories, Integer maxLevel) {
        ObjectNode rootNode = JsonNodeFactory.instance.objectNode();
        if (maxLevel == null) {
            maxLevel = 5;
        }
        for (ApCategory category : categories) {
            ObjectNode node = rootNode;
            int index = 1;
            do {
                node = buildCatNode(node, category.getCatByIndex(index++), category.getId());
            } while (node != null && index <= maxLevel);
        }

        return rootNode;
    }

    private static ObjectNode buildCatNode(ObjectNode parent, String cat, Long id) {
        if (StringUtils.isBlank(cat)) {
            parent.put("id", id);
            return null;
        } else {
            if (parent.has(cat)) {
                return (ObjectNode) parent.get(cat);
            } else {
                ObjectNode child = JsonNodeFactory.instance.objectNode();
                parent.set(cat, child);
                return child;
            }
        }
    }
}
