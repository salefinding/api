package com.salefinding.api.utils;

public interface Constants {

    String DB_API = "default";

    String OK = "OK";

    String YES = "YES";

    String NO = "NO";

    String BOOLEAN_TRUE = "true";

    String BOOLEAN_FALSE = "false";

    String STATUS = "status";

    String MESSAGE = "message";

    String NO_UPDATE = "NO_UPDATE";

    String SUCCESS = "success";

    String FAIL = "fail";

    String PROP_FILE_NAME = "app.properties";

}
