package com.salefinding.api.utils;

import play.mvc.Http;

public interface HttpContextModule {
    default Http.Context getHttpContext() {
        return Http.Context.current();
    }
}
