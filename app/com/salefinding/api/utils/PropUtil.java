package com.salefinding.api.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropUtil {

    private static final Logger logger = LoggerFactory.getLogger(PropUtil.class);

    private static Properties prop = null;

    private synchronized static void init() {
        try {
            if (prop != null) {
                return;
            }
            prop = new Properties();
            prop.load(new FileInputStream(Constants.PROP_FILE_NAME));
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public static String getPropertyValue(String key) {
        return getPropertyValue(key, null);
    }

    public static String getPropertyValue(String key, String defaultValue) {
        if (prop == null) {
            init();
        }
        return prop.getProperty(key, defaultValue);
    }
}

