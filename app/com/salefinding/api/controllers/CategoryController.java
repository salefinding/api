package com.salefinding.api.controllers;

import com.salefinding.api.utils.EntityToJson;
import com.salefinding.models.api.ApCategory;
import com.salefinding.repositories.ApiRepoFactory;
import play.Logger;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class CategoryController extends BaseController {

    private Logger.ALogger logger = Logger.of(CategoryController.class);

    @Inject
    private ApiRepoFactory apiRepoFactory;

    public Result getCategoryByBranch(Long branchId) {
        try {
            List<ApCategory> categories = apiRepoFactory.getCategoryRepository().getByBranchCountryId(branchId);
            categories.removeIf(category -> apiRepoFactory.getItemRepository().getByCatId(category.getId()).size() ==
                                            0);
            return ok(EntityToJson.parseListCategory(categories, null));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return badRequest();
        }
    }

    public Result getCategoryByBranchIdAndLevel(Long branchId, Integer level) {
        try {
            List<ApCategory> categories = apiRepoFactory.getCategoryRepository().getByBranchCountryId(branchId);
            categories.removeIf(category -> apiRepoFactory.getItemRepository().getByCatId(category.getId()).size() ==
                                            0);

            return ok(EntityToJson.parseListCategory(categories, level));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return badRequest();
        }
    }

}
