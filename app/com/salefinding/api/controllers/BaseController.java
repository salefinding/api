package com.salefinding.api.controllers;

import com.salefinding.api.utils.HttpContextModule;
import play.mvc.Controller;

public abstract class BaseController extends Controller implements HttpContextModule {

}
