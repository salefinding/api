package com.salefinding.api.controllers;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.salefinding.models.api.ApCategory;
import com.salefinding.models.api.ApItem;
import com.salefinding.models.api.ApItemCountryDetail;
import com.salefinding.repositories.ApiRepoFactory;
import play.Logger;
import play.libs.Json;
import play.mvc.Result;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ItemController extends BaseController {

    private Logger.ALogger logger = Logger.of(ItemController.class);

    @Inject
    private ApiRepoFactory apiRepoFactory;

    public Result getItemByCatId(Long catId) {
        Collection<ApItem> items = apiRepoFactory.getItemRepository().getByCatId(catId);
        return ok(buildListItem(items));
    }

    public Result searchByNameOrCode(String input) {
        Collection<ApItem> items = apiRepoFactory.getItemRepository().searchByNameOrCode(input);
        return ok(buildListItem(items));
    }

    public Result getTopSale() {
        Collection<ApItem> items = apiRepoFactory.getItemRepository().getTopSaleRate(50);
        return ok(buildListItem(items));
    }

    public Result getItemByCat1(Long branchId, String cat1) {
        return getItemByCat1AndCat2(branchId, cat1, null);
    }

    public Result getItemByCat1AndCat2(Long branchId, String cat1, String cat2) {
        return getItemByCat1AndCat2AndCat3(branchId, cat1, cat2, null);
    }

    public Result getItemByCat1AndCat2AndCat3(Long branchId, String cat1, String cat2, String cat3) {
        List<ApCategory> categories = apiRepoFactory.getCategoryRepository()
                                                    .getByCats(branchId, cat1, cat2, cat3, null, null);

        List<Long> catIds = categories.parallelStream().map(ApCategory::getId).collect(Collectors.toList());

        List<ApItem> items = apiRepoFactory.getItemRepository().getByCatIds(catIds);

        return ok(buildListItem(items));
    }

    private ArrayNode buildListItem(Collection<ApItem> items) {
        ArrayNode arrayNode = JsonNodeFactory.instance.arrayNode();
        try {
            for (ApItem item : items) {
                ObjectNode node = Json.newObject();
                node.put(ApItem.IdColumn, item.getId());
                node.put(ApItem.NameColumn, item.getName());
                node.put(ApItem.CodeColumn, item.getCode());

                ArrayNode details = Json.newArray();
                int maxSaleRate = 0;
                for (ApItemCountryDetail itemDetail : item.getItemDetailList()) {
                    node.put(ApItemCountryDetail.ImageColumn, itemDetail.getImage());
                    if (itemDetail.getSaleRate() > 0) {
                        ObjectNode detail = Json.newObject();
                        detail.put(ApItemCountryDetail.PriceColumn,
                                   itemDetail.getPrice()
                                             .multiply(itemDetail.getCountry().getCurrency().getVndExchangeRate())
                                             .multiply(BigDecimal.valueOf(1.1))
                                             .divideToIntegralValue(BigDecimal.valueOf(100))
                                             .multiply(BigDecimal.valueOf(100))
                                             .intValue());
                        detail.put(ApItemCountryDetail.OldPriceColumn,
                                   itemDetail.getOldPrice()
                                             .multiply(itemDetail.getCountry().getCurrency().getVndExchangeRate())
                                             .multiply(BigDecimal.valueOf(1.1))
                                             .divideToIntegralValue(BigDecimal.valueOf(100))
                                             .multiply(BigDecimal.valueOf(100))
                                             .intValue());
                        detail.put(ApItemCountryDetail.UrlColumn, itemDetail.getUrl());
                        detail.put("country", itemDetail.getCountry().getName());

                        detail.put(ApItemCountryDetail.SaleRateColumn, itemDetail.getSaleRate());
                        maxSaleRate = itemDetail.getSaleRate() > maxSaleRate ? itemDetail.getSaleRate() : maxSaleRate;

                        details.add(detail);
                    }
                }

                /*ArrayNode stocks = Json.newArray();
                for (ApItemStock itemStock : item.getItemStockList()) {
                    if (itemStock.getStatus() && itemStock.getStock() > 0) {
                        ObjectNode stock;
                        if (stocks.get(itemStock.getColorCode()) != null) {
                            stock = (ObjectNode) stocks.get(itemStock.getColorCode());
                        } else {
                            stock = Json.newObject();
                        }
                        stock.put("size_code", itemStock.getSizeCode());
                        //stocks.
                    }
                }*/

                node.put("max_sale_rate", maxSaleRate);
                node.set("details", details);
                //node.set("stocks", stocks);
                arrayNode.add(node);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return arrayNode;
    }
}
