package com.salefinding.api.controllers;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.salefinding.api.utils.EntityToJson;
import com.salefinding.models.api.ApBranch;
import com.salefinding.models.api.ApCategory;
import com.salefinding.repositories.ApiRepoFactory;
import play.Logger;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class BranchController extends BaseController {

    private Logger.ALogger logger = Logger.of(BranchController.class);

    @Inject
    private ApiRepoFactory apiRepoFactory;

    public Result getAllBranch() {
        try {
            List<ApBranch> allBranches = apiRepoFactory.getBranchRepository().getAll();

            ArrayNode arrayNode = JsonNodeFactory.instance.arrayNode();
            for (ApBranch branch : allBranches) {
                ObjectNode node = JsonNodeFactory.instance.objectNode();
                node.put(ApBranch.IdColumn, branch.getId());
                node.put(ApBranch.NameColumn, branch.getName());

                List<ApCategory> categories = apiRepoFactory.getCategoryRepository()
                                                            .getByBranchCountryId(branch.getId());
                categories.removeIf(category ->
                                            apiRepoFactory.getItemRepository().getByCatId(category.getId()).size() ==
                                            0);

                node.set("categories", EntityToJson.parseListCategory(categories, null));
                arrayNode.add(node);
            }
            return ok(arrayNode);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return badRequest();
        }
    }
}
