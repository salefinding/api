package com.salefinding.api.dtos;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.salefinding.api.utils.Constants;
import play.mvc.Result;

import static play.mvc.Results.badRequest;
import static play.mvc.Results.ok;

public class SFResponse {

    public static Result success() {
        return success(null);
    }

    public static Result success(String message) {
        ObjectNode jsonNode = baseObjectNode(Constants.SUCCESS, message);
        return ok(jsonNode);
    }

    public static Result fail() {
        return fail(null);
    }

    public static Result fail(String message) {
        ObjectNode jsonNode = baseObjectNode(Constants.FAIL, message);
        return badRequest(jsonNode);
    }

    private static ObjectNode baseObjectNode(String status, String message) {
        ObjectNode objectNode = JsonNodeFactory.instance.objectNode();
        objectNode.put(Constants.STATUS, status);
        objectNode.put(Constants.MESSAGE, message);

        return objectNode;
    }

}
