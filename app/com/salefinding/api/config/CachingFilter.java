package com.salefinding.api.config;

import akka.stream.Materializer;
import com.salefinding.api.controllers.BranchController;
import com.salefinding.api.utils.PropUtil;
import play.Logger;
import play.mvc.Filter;
import play.mvc.Http;
import play.mvc.Result;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

import static play.mvc.Results.ok;

public class CachingFilter extends Filter {

    private static Logger.ALogger logger = Logger.of(BranchController.class);

    @Inject
    private JedisPool jedisPool;

    @Inject
    public CachingFilter(Materializer materializer) {
        super(materializer);
    }

    @Override
    public CompletionStage<Result> apply(Function<Http.RequestHeader, CompletionStage<Result>> function,
                                         Http.RequestHeader requestHeader) {
        try (Jedis jedis = jedisPool.getResource()) {
            boolean redisCacheEnable = Boolean.parseBoolean(PropUtil.getPropertyValue("redis.cache.enable", "false"));

            if (redisCacheEnable && jedis.exists(requestHeader.uri())) {
                logger.info("Load from cache for {}", requestHeader.uri());
                return CompletableFuture.completedFuture(ok(jedis.get(requestHeader.uri())));
            }
            return function.apply(requestHeader).thenApply(result -> {
                result.body().consumeData(materializer).thenApply(responseString -> {
                    logger.info("Save to cache for {}", requestHeader.uri());
                    jedis.set(requestHeader.uri(), responseString.utf8String());
                    return responseString;
                });

                return result;
            });
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return function.apply(requestHeader);
        }
    }
}
