package com.salefinding.api.config;

import com.google.inject.AbstractModule;
import com.salefinding.api.utils.PropUtil;
import com.salefinding.repositories.ApiRepoFactory;
import com.salefinding.utils.SFObjectMapper;
import play.libs.Json;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import static com.salefinding.api.utils.Constants.DB_API;

public class CustomModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ApiRepoFactory.class).toInstance(ApiRepoFactory.getInstance(DB_API));

        Json.setObjectMapper(new SFObjectMapper(true));

        String redisHost = PropUtil.getPropertyValue("redis.host", "localhost");
        bind(JedisPool.class).toInstance(new JedisPool(new JedisPoolConfig(), redisHost));
    }

}
