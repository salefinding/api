package com.salefinding.api.config;

import com.google.inject.Inject;
import play.api.http.EnabledFilters;
import play.http.DefaultHttpFilters;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CustomFilter extends DefaultHttpFilters {

    @Inject
    public CustomFilter(CachingFilter cachingFilter, EnabledFilters enabledFilters) {
        super(Stream.concat(enabledFilters.asJava().getFilters().stream(), Stream.of(cachingFilter)).collect(Collectors.toList()));
    }
}
