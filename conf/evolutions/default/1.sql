# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table branch (
  id                            bigint auto_increment not null,
  name                          varchar(200),
  url                           varchar(200),
  uuid                          varchar(32),
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint pk_branch primary key (id)
);

create table category (
  id                            bigint auto_increment not null,
  branch_id                     bigint not null,
  cat1                          varchar(200),
  cat2                          varchar(200),
  cat3                          varchar(200),
  cat4                          varchar(200),
  cat5                          varchar(200),
  available                     tinyint(1) not null,
  uuid                          varchar(32) not null,
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint pk_category primary key (id)
);

create table country (
  id                            bigint auto_increment not null,
  name                          varchar(200),
  short_code                    varchar(45),
  currency_id                   bigint not null,
  uuid                          varchar(32),
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint uq_country_currency_id unique (currency_id),
  constraint pk_country primary key (id)
);

create table currency (
  id                            bigint auto_increment not null,
  name                          varchar(200),
  short_code                    varchar(3),
  vnd_exchange_rate             decimal(38),
  uuid                          varchar(32),
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint pk_currency primary key (id)
);

create table item (
  id                            bigint auto_increment not null,
  branch_id                     bigint not null,
  code                          varchar(200) not null,
  name                          varchar(200) not null,
  uuid                          varchar(32),
  max_sale_rate                 integer not null,
  available                     tinyint(1) not null,
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint pk_item primary key (id)
);

create table item_country_detail (
  id                            bigint auto_increment not null,
  item_id                       bigint not null,
  branch_id                     bigint not null,
  country_id                    bigint not null,
  category_id                   bigint not null,
  code                          varchar(200) not null,
  name                          varchar(200) not null,
  image                         varchar(65535) not null,
  old_price                     decimal(38),
  price                         decimal(38) not null,
  special_price                 decimal(38),
  sale_rate                     integer not null,
  url                           varchar(65535) not null,
  available                     tinyint(1) not null,
  uuid                          varchar(32) not null,
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint pk_item_country_detail primary key (id)
);

create table item_image (
  id                            bigint auto_increment not null,
  item_detail_id                bigint not null,
  url                           varchar(65535) not null,
  description                   varchar(65535),
  uuid                          varchar(32),
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint pk_item_image primary key (id)
);

create table item_stock (
  id                            bigint auto_increment not null,
  item_detail_id                bigint not null,
  color_code                    varchar(100),
  size_code                     varchar(100),
  stock                         integer not null,
  status                        tinyint(1) not null,
  uuid                          varchar(32) not null,
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint pk_item_stock primary key (id)
);

create table order (
  id                            bigint auto_increment not null,
  user_id                       bigint not null,
  user_address_id               bigint not null,
  order_detail                  varchar(65535),
  total_amount                  decimal(38),
  status                        integer not null,
  payment_status                integer not null,
  uuid                          varchar(32),
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint pk_order primary key (id)
);

create table user (
  id                            bigint auto_increment not null,
  email                         varchar(200) not null,
  password                      varchar(200) not null,
  name                          varchar(200),
  date_of_birth                 date,
  total_spent_money             decimal(38),
  is_blocked                    tinyint(1) not null,
  is_actived                    tinyint(1) not null,
  uuid                          varchar(32),
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint pk_user primary key (id)
);

create table user_address (
  id                            bigint auto_increment not null,
  user_id                       bigint not null,
  address                       varchar(500),
  phone                         date,
  country                       varchar(45),
  is_default                    tinyint(1) not null,
  uuid                          varchar(32),
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint pk_user_address primary key (id)
);

create table user_item_subcribe (
  id                            bigint auto_increment not null,
  user_id                       bigint not null,
  entity_type                   varchar(50) not null,
  entity_id                     bigint not null,
  status                        tinyint(1) not null,
  created_at                    datetime(6) not null,
  updated_at                    datetime(6) not null,
  constraint pk_user_item_subcribe primary key (id)
);

create index ix_category_branch_id on category (branch_id);
alter table category add constraint fk_category_branch_id foreign key (branch_id) references branch (id) on delete restrict on update restrict;

alter table country add constraint fk_country_currency_id foreign key (currency_id) references currency (id) on delete restrict on update restrict;

create index ix_item_branch_id on item (branch_id);
alter table item add constraint fk_item_branch_id foreign key (branch_id) references branch (id) on delete restrict on update restrict;

create index ix_item_country_detail_item_id on item_country_detail (item_id);
alter table item_country_detail add constraint fk_item_country_detail_item_id foreign key (item_id) references item (id) on delete restrict on update restrict;

create index ix_item_country_detail_country_id on item_country_detail (country_id);
alter table item_country_detail add constraint fk_item_country_detail_country_id foreign key (country_id) references country (id) on delete restrict on update restrict;

create index ix_item_country_detail_category_id on item_country_detail (category_id);
alter table item_country_detail add constraint fk_item_country_detail_category_id foreign key (category_id) references category (id) on delete restrict on update restrict;

create index ix_item_image_item_detail_id on item_image (item_detail_id);
alter table item_image add constraint fk_item_image_item_detail_id foreign key (item_detail_id) references item_country_detail (id) on delete restrict on update restrict;

create index ix_item_stock_item_detail_id on item_stock (item_detail_id);
alter table item_stock add constraint fk_item_stock_item_detail_id foreign key (item_detail_id) references item_country_detail (id) on delete restrict on update restrict;


# --- !Downs

alter table category drop foreign key fk_category_branch_id;
drop index ix_category_branch_id on category;

alter table country drop foreign key fk_country_currency_id;

alter table item drop foreign key fk_item_branch_id;
drop index ix_item_branch_id on item;

alter table item_country_detail drop foreign key fk_item_country_detail_item_id;
drop index ix_item_country_detail_item_id on item_country_detail;

alter table item_country_detail drop foreign key fk_item_country_detail_country_id;
drop index ix_item_country_detail_country_id on item_country_detail;

alter table item_country_detail drop foreign key fk_item_country_detail_category_id;
drop index ix_item_country_detail_category_id on item_country_detail;

alter table item_image drop foreign key fk_item_image_item_detail_id;
drop index ix_item_image_item_detail_id on item_image;

alter table item_stock drop foreign key fk_item_stock_item_detail_id;
drop index ix_item_stock_item_detail_id on item_stock;

drop table if exists branch;

drop table if exists category;

drop table if exists country;

drop table if exists currency;

drop table if exists item;

drop table if exists item_country_detail;

drop table if exists item_image;

drop table if exists item_stock;

drop table if exists order;

drop table if exists user;

drop table if exists user_address;

drop table if exists user_item_subcribe;

