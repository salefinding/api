lazy val commonSettings = Seq(
  organization := "com.salefinding",
  version := "0.1.0-SNAPSHOT"
)

scalaVersion := "2.12.6"

crossScalaVersions := Seq("2.11.12", "2.12.4")

routesGenerator := InjectedRoutesGenerator

lazy val models = ProjectRef(file("../model"), "models")

lazy val api = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    name := "api",
    libraryDependencies ++= Seq(
      javaWs,
      specs2 % Test,
      guice,
      "redis.clients" % "jedis" % "2.9.0",
      "commons-net" % "commons-net" % "3.6",
      "mysql" % "mysql-connector-java" % "8.0.11"
    )
  )
  .enablePlugins(PlayJava)
  .aggregate(models)
  .dependsOn(models)